package x.core;

import java.awt.*;

public class System_message implements Message {


    @Override
    public void drawSelf(Graphics2D g, int x, int y)
    {
        Graphics2D g2 = (Graphics2D)g.create();
        g2.setColor(systextclr);
        g2.drawString (text, x, y);

    }

    private static final Color systextclr = Color.gray;

    public System_message(String text){

        this.text = text;
    }

    private final String text;
}
