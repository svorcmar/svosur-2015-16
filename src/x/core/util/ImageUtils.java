package x.core.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ImageUtils {

    public static final boolean DEBUG_TEXTURES = false;

    private final Map<String, BufferedImage> rawCache = new HashMap<>();
    private final Map<String, BufferedImage> scaledCache = new HashMap<>();
    private final double scaleRatio;

    public ImageUtils(double scaleRatio) {
        this.scaleRatio = scaleRatio;
    }

    public BufferedImage getImage(String name) {
        BufferedImage image;
        if ((image = rawCache.get(name)) == null) {
            image = getFromFile(name);
            rawCache.put(name, image);
        }
        return image;
    }

    public Image getScaledImage(String name) {
        BufferedImage image;
        if ((image = scaledCache.get(name)) == null) {
            image = getFromFile(name);
            if (image != null) {
                double scale = scaleRatio;
                while (scale < 0.5) {
                    image = scale(image, 0.5);
                    scale *= 2;
                }
                image = scale(image, scale);
                scaledCache.put(name, image);
                return image;
            }
        }
        return image;
    }

    private void postProcess(BufferedImage image) {
        if (DEBUG_TEXTURES) {
            image.setRGB(0, 0, Color.RED.getRGB());
//            image.setRGB(1, 1, Color.RED.getRGB());
            image.setRGB(0, image.getHeight() - 1, Color.BLUE.getRGB());
//            image.setRGB(1, image.getHeight() - 2, Color.BLUE.getRGB());
            image.setRGB(image.getWidth() - 1, 0, Color.GREEN.getRGB());
//            image.setRGB(image.getWidth() - 2, 1, Color.GREEN.getRGB());
            image.setRGB(image.getWidth() - 1, image.getHeight() - 1, Color.ORANGE.getRGB());
//            image.setRGB(image.getWidth() - 2, image.getHeight() - 2, Color.ORANGE.getRGB());
        }
    }

    private BufferedImage scale(BufferedImage image, double ratio) {
        int w = image.getWidth();
        int h = image.getHeight();
        BufferedImage after = new BufferedImage((int) Math.ceil(w * ratio), (int) Math.ceil(h * ratio), BufferedImage.TYPE_INT_ARGB);
        AffineTransform at = new AffineTransform();
        at.scale(ratio, ratio);
        AffineTransformOp scaleOp =
                new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
        return scaleOp.filter(image, after);
    }

    private BufferedImage getFromFile(String name) {
        InputStream input = ImageUtils.class.getClassLoader().getResourceAsStream("./images/" + name);
        if (input == null)
            return null;
        try {
            BufferedImage read = ImageIO.read(input);
            postProcess(read);
            return read;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
