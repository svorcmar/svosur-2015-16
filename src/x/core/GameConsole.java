package x.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GameConsole {

    private final List<Message> content = new ArrayList<>();

    public void addMessage(Message text) {
        content.add(text);
    }

    public List<Message> getContent() {
        return content;
    }
}

