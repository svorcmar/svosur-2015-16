package x.core.floor;

import x.core.*;
import x.core.entity.*;
import x.core.light.CircularLightSource;
import x.core.light.LightSource;

import java.util.*;

public class Dungeon {

    private final int width;
    private final int height;
    private final Coord playerCoord = new Coord(0, 0);
    private final List<LightSource> lightSources = new ArrayList<>();
    // pole skutecne videna hracem (neprerusena prekazkou)
    private final boolean[][] visibleSquares;
    private DungeonSquare[][] squares;

    private final long seed = new Random().nextLong();
    private final Random random = new Random(seed);

    private final List<Ticking> tickings = new ArrayList<>();

    private boolean shouldRecomputeLighting = true;
    private final Game game;
    private final String name;

    public Dungeon(Game game, int width, int height, String name) {
        this.game = game;
        this.width = width;
        this.height = height;
        this.name = name;
        squares = new DungeonSquare[width][height];
        visibleSquares = new boolean[height][width];
        Entity wall = new Wall();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                squares[i][j] = new DungeonSquareImpl();
                squares[i][j].addEntity(wall);
            }
        }
        // zapnout pro vygenerovani dvou bublin a svetla navic
        twoBubbles();
//        generateFloor();
        squares[playerCoord.x][playerCoord.y].addEntity(new Player());
        // todo - lighting
        addLightSource(new CircularLightSource(10, playerCoord));
    }

    public Dungeon(Game game, DungeonSquare[][] dungeonSquares, String name) {
        this.game = game;
        this.squares = dungeonSquares;
        this.name = name;
        this.width = dungeonSquares.length;
        this.height = dungeonSquares[0].length;
        visibleSquares = new boolean[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (dungeonSquares[i][j].hasEntity(EntityType.PLAYER)) {
                    playerCoord.x = i;
                    playerCoord.y = j;
                }
                synchronized (tickings) {
                    dungeonSquares[i][j].getEntities().stream().filter(e -> e instanceof Ticking).forEach(e -> tickings.add((Ticking) e));
                }
            }
        }
        addLightSource(new CircularLightSource(3, playerCoord));
    }

    public void tick() {
        synchronized (tickings) {
            for (Iterator<Ticking> iterator = tickings.iterator(); iterator.hasNext(); ) {
                Ticking ticking = iterator.next();
                if (ticking.isAlive()) {
                    ticking.tick(this);
                } else {
                    iterator.remove();
                }
            }
        }
    }


    private void twoBubbles() {
        // todo - centered player
        playerCoord.x = width / 2;
        playerCoord.y = height / 2;
        // todo - random walls
        carveCircle(20, playerCoord.x, playerCoord.y);
        carveCircle(width / 6, width / 4, height / 2);
        addLightSource(new CircularLightSource(20, 40, 40));
    }

    private void addLightSource(LightSource lightSource) {
        lightSources.add(lightSource);
        updateVisibilities();
    }

    private void generateFloor() {
        HashSet<Coord> forbidden = new HashSet<>();
        Coord player = generatePoint(forbidden);
//        forbidden.add(player);
        playerCoord.x = player.x;
        playerCoord.y = player.y;
        Coord end = generatePoint(forbidden);
//        generatePath(playerCoord, end, width + height);
        List<Coord> path = generatePath(player, end, forbidden, 50);
        if (path != null)
            path.forEach(c -> squares[c.x][c.y].removeEntity(EntityType.WALL));
    }

    private List<Coord> generatePath(Coord from, Coord to, Set<Coord> forbidden, int length) {
        int totalLength = 0;
        List<Coord> returnPath = new ArrayList<>();
        List<Coord> path;
        // je cesta ok?
        do {
            // cesta skrz dalsi bod
            Coord interPoint = generatePoint(forbidden);
            List<Coord> permaPath = findShortestPath(from, interPoint, forbidden);
            if (permaPath == null) {
                throw new IllegalStateException("no path found");
            }
            // prvni cesta bude permanentni
            forbidden.addAll(permaPath);
            returnPath.addAll(permaPath);
            totalLength += permaPath.size();
            // druha polovina cesty
            path = findShortestPath(interPoint, to, forbidden);
            if (path == null) {
                throw new IllegalStateException("no path found");
            }
            from = interPoint;
        } while (totalLength + path.size() < length);
        returnPath.addAll(path);
        return returnPath;
    }

    private Coord generatePoint(Set<Coord> forbidden) {
        int x, y;
        do {
            x = random.nextInt(width);
            y = random.nextInt(height);
        } while (!isOk(x, y, forbidden));
        return new Coord(x, y);
    }

    // A* algoritmus nalezeni nejkratsi cesty
    private List<Coord> findShortestPath(Coord from, Coord to, Set<Coord> futurePaths) {
        class CoordWithPrevious extends Coord {
            CoordWithPrevious previous;

            public CoordWithPrevious(int x, int y, CoordWithPrevious previous) {
                super(x, y);
                this.previous = previous;
            }
        }

        // prioritni fronta nam da vzdycky souradnici, ktera vypada, ze je nejbliz
        // to, ze je nejbliz, ji musime spocitat, proto ji predavame komparator
        PriorityQueue<CoordWithPrevious> pq = new PriorityQueue<>(new Comparator<Coord>() {
            @Override
            public int compare(Coord o1, Coord o2) {
                int diff = Math.abs(o1.x - to.x) + Math.abs(o1.y - to.y)
                        - (Math.abs(o2.x - to.x) + Math.abs(o2.y - to.y));
                // pokud jsou stejne daleko, randomizujeme dalsi prvek odebrany z fronty
                return diff != 0 ? diff : random.nextInt();
            }
        });
        CoordWithPrevious start = new CoordWithPrevious(from.x, from.y, null);
        pq.add(start);
        // mnozina souradnic, ktere jsme uz pridali do fronty a tedy byly / budou vyzkouseny
        Set<Coord> explored = new HashSet<>();
        explored.add(start);
        CoordWithPrevious found = null;
        while (!pq.isEmpty()) {
            CoordWithPrevious c = pq.poll();

            // todo - zakomentovany vypis prubehu algoritmu
//            for (int i = 0; i < width; i++) {
//                for (int j = 0; j < height; j++) {
//                    CoordWithPrevious t = new CoordWithPrevious(i, j, null);
//                    if (start.equals(t)) {
//                        System.out.print("S");
//                    } else if (to.equals(t)) {
//                        System.out.print("X");
//                    } else if (t.equals(c)) {
//                        System.out.print("*");
//                    } else if (explored.contains(t)) {
//                        System.out.print(" ");
//                    } else {
//                        System.out.print("-");
//                    }
//                }
//                System.out.println();
//            }
//            System.out.println();


            if (c.equals(to)) {
                found = c;
                break;
            }
            CoordWithPrevious next;
            if (isOk(c.x + 1, c.y, futurePaths)) {
                next = new CoordWithPrevious(c.x + 1, c.y, c);
                if (!explored.contains(next)) {
                    pq.add(next);
                    explored.add(next);
                }
            }
            if (isOk(c.x - 1, c.y, futurePaths)) {
                next = new CoordWithPrevious(c.x - 1, c.y, c);
                if (!explored.contains(next)) {
                    pq.add(next);
                    explored.add(next);
                }
            }
            if (isOk(c.x, c.y + 1, futurePaths)) {
                next = new CoordWithPrevious(c.x, c.y + 1, c);
                if (!explored.contains(next)) {
                    pq.add(next);
                    explored.add(next);
                }
            }
            if (isOk(c.x, c.y - 1, futurePaths)) {
                next = new CoordWithPrevious(c.x, c.y - 1, c);
                if (!explored.contains(next)) {
                    pq.add(next);
                    explored.add(next);
                }
            }

        }
        // nenasli jsme, vracime null
        if (found == null)
            return null;
        // jinak jsme nasli a jdeme od konce a pridavame do seznamu, takze je pozpatku
        List<Coord> path = new ArrayList<>();
        // ignorovat posledni krok
        found = found.previous;
        while (found != null) {
            path.add(found);
            found = found.previous;
        }
        // proto ho musime jeste otocit
        Collections.reverse(path);
        return path;
    }

    private boolean isOk(int x, int y, Set<Coord> forbidden) {
        if (!isWithin(x, y))
            return false;
        if (!squares[x][y].hasEntity(EntityType.WALL))
            return false;
        if (!isWithin(x + 1, y) || !squares[x + 1][y].hasEntity(EntityType.WALL) || forbidden.contains(new Coord(x + 1, y)))
            return false;
        if (!isWithin(x - 1, y) || !squares[x - 1][y].hasEntity(EntityType.WALL) || forbidden.contains(new Coord(x - 1, y)))
            return false;
        if (!isWithin(x, y + 1) || !squares[x][y + 1].hasEntity(EntityType.WALL) || forbidden.contains(new Coord(x, y + 1)))
            return false;
        if (!isWithin(x, y - 1) || !squares[x][y - 1].hasEntity(EntityType.WALL) || forbidden.contains(new Coord(x, y - 1)))
            return false;
        return true;
    }

    public void updateVisibilities() {
        // vsechno vytmavime
        for (DungeonSquare[] row : squares) {
            for (DungeonSquare square : row) {
                square.setLightIntensity(0, 0);
            }
        }
        // a pak pro kazdy zdroj svetla vybarvime
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                for (LightSource lightSource : lightSources) {
                    squares[i][j].setLightIntensity(lightSource.getIntensity(i, j), 1);
                }
            }
        }
    }

    public void action() {
        for (Entity entity : squares[playerCoord.x][playerCoord.y].getEntities()) {
            entity.action(this);
        }
    }

    private void carveCircle(int radius, int x, int y) {
        for (int i = -radius; i <= radius; i++) {
            for (int j = -radius; j <= radius; j++) {
                double diff = Math.sqrt(i * i + j * j) - Math.sqrt(radius * radius);
                if (diff < 0.5) {
                    int dx = x + i;
                    int dy = y + j;
                    if (isWithin(dx, dy)) {
                        DungeonSquare square = squares[dx][dy];
                        int wallsRemoved = square.removeEntity(EntityType.WALL);
                        square.addEntity(new Walkable());
                        assert wallsRemoved <= 1;
                    }
                }
            }
        }
    }

    public boolean isWithin(int x, int y) {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    public Coord getPlayer() {
        return playerCoord;
    }

    public void movePlayer(int dx, int dy) {
        if (isWithin(playerCoord.x + dx, playerCoord.y + dy)) {
            DungeonSquare targetSquare = squares[playerCoord.x + dx][playerCoord.y + dy];
            // temporary list - double iteration
            ArrayList<Entity> entities = new ArrayList<>(targetSquare.getEntities());
            // iterate from the topmost entity
            for (int i = entities.size() - 1; i >= 0; i--) {
                if (entities.get(i).playerEntered(dx, dy, this))
                    break;
            }
        }
    }

    public void transferPlayer(int newX, int newY) {
        Entity player = squares[playerCoord.x][playerCoord.y].getEntities().stream().filter(e -> e.getType() == EntityType.PLAYER).findAny().get();
        squares[playerCoord.x][playerCoord.y].removeEntity(player);
        playerCoord.x = newX;
        playerCoord.y = newY;
        squares[playerCoord.x][playerCoord.y].addEntity(player);
        assert isWithin(playerCoord.x, playerCoord.y);
        shouldRecomputeLighting = true;
    }


    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public DungeonSquare get(int i, int j) {
        return squares[i][j];
    }

    public boolean shouldRecomputeLighting() {
        return shouldRecomputeLighting;
    }

    public void lightingRecomputed() {
        shouldRecomputeLighting = false;
    }

    public void computeLighting(int fromX, int fromY) {
        final int rayCount = 1000;
        for (int i = 0; i < rayCount; i++) {
            double radians = Math.PI * 2 / rayCount * i;
            double x = fromX + Math.cos(radians);
            double y = fromY + Math.sin(radians);
            int dx = (x - fromX > 0 ? 1 : (x - fromX < 0 ? -1 : 0));
            int dy = (y - fromY > 0 ? 1 : (y - fromY < 0 ? -1 : 0));
            int nextX = fromX;
            int nextY = fromY;
            squares[nextX][nextY].setExplored(true);
            boolean shouldEnd = false;
            while (!shouldEnd) {
                visibleSquares[nextX][nextY] = true;
                // vector product
                if (dx != 0 && intersects(fromX, fromY, x, y, nextX + dx, nextY)) {
                    nextX += dx;
                } else if (dy != 0 && intersects(fromX, fromY, x, y, nextX, nextY + dy)) {
                    nextY += dy;
                } else if (dx != 0 && dy != 0 && intersects(fromX, fromY, x, y, nextX + dx, nextY + dy)) {
                    nextX += dx;
                    nextY += dy;
                } else {
                    // ray does not continue? impossible
                    throw new IllegalStateException("RayCasting error");
                }
                if (!isWithin(nextX, nextY))
                    // out of grid
                    break;
                if (squares[nextX][nextY].isOpaque()) {
                    // opaque, the ray ends here
                    shouldEnd = true;
                    visibleSquares[nextX][nextY] = true;
                }
                if (squares[nextX][nextY].getLightIntensity() > 0)
                    squares[nextX][nextY].setExplored(true);
            }
        }
    }

    private static boolean intersects(double x0, double y0, double x1, double y1, double centerX, double centerY) {
        double vx = x1 - x0;
        double vy = y1 - y0;
        double sgnVec = Math.signum(vx * (centerY - 0.5 - y0) - vy * (centerX - 0.5 - x0));
        if (Math.signum(vx * (centerY - 0.5 - y0) - vy * (centerX + 0.5 - x0)) != sgnVec)
            return true;
        if (Math.signum(vx * (centerY + 0.5 - y0) - vy * (centerX - 0.5 - x0)) != sgnVec)
            return true;
        if (Math.signum(vx * (centerY + 0.5 - y0) - vy * (centerX + 0.5 - x0)) != sgnVec)
            return true;
        return false;
    }

    public void clearLighting() {
        for (boolean[] row : visibleSquares) {
            Arrays.fill(row, false);
        }
    }

    public boolean lineOfSightBlocked(int x, int y) {
        return !visibleSquares[x][y];
    }

    public Game getGame() {
        return game;
    }

    public String getName() {
        return name;
    }
}
