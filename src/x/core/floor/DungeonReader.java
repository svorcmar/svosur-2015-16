package x.core.floor;

import x.core.*;
import x.core.enemy.SimpleEnemy;
import x.core.entity.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DungeonReader {

    private final BufferedReader reader;
    private Game game;

    public DungeonReader(BufferedReader reader, Game game) {
        this.reader = reader;
        this.game = game;
    }

    public Dungeon readFloor() throws IOException {
        List<DungeonSquare[]> list = new ArrayList<>();
        // read name first
        String name = reader.readLine();
        String line = reader.readLine();
        int expectedSize = line.length();
        int playerCount = 0;
        while (line != null) {
            if (line.length() != expectedSize) {
                throw new IllegalArgumentException("Uneven row lengths");
            }
            DungeonSquare[] row = new DungeonSquare[expectedSize];
            for (int i = 0; i < expectedSize; i++) {
                row[i] = new DungeonSquareImpl();
                Entity entity;
                boolean walkable;
                switch (line.charAt(i)) {
                    case ' ':
                        entity = null;
                        walkable = true;
                        break;
                    case 'X':
                        entity = new SimpleEnemy();
                        walkable = true;
                        break;
                    case 'W':
                        entity = new Wall();
                        walkable = false;
                        break;
                    case 'P':
                        ++playerCount;
                        walkable = true;
                        entity = new Player();
                        break;
                    case 'w':
                        entity = new Water();
                        walkable = true;
                        break;
                    case 'S':
                        walkable = true;
                        entity = new Stairs(new Dungeon(game, 20, 20, "Random dungeon"), new Coord(10, 10));
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown entity code: '" + line.charAt(i) + "'");
                }
                if (walkable)
                    row[i].addEntity(new Walkable());
                row[i].addEntity(entity);
            }
            list.add(row);
            line = reader.readLine();
        }
        if (playerCount != 1) {
            throw new IllegalArgumentException("Exctly one player expected, got " + playerCount);
        }
        // transponovat patro
        DungeonSquare[][] dungeonSquares = list.toArray(new DungeonSquare[list.size()][]);
        DungeonSquare[][] transposed = new DungeonSquare[expectedSize][dungeonSquares.length];
        for (int i = 0; i < dungeonSquares.length; i++) {
            for (int j = 0; j < expectedSize; j++) {
                transposed[j][dungeonSquares.length - i - 1] = dungeonSquares[i][j];
            }
        }
        return new Dungeon(game, transposed, name);
    }
}
