package x.core;


import x.core.floor.Dungeon;

public class NewFloorMsg extends System_message {
    public NewFloorMsg(Dungeon dungeon) {
        super("Entered [" + dungeon.getName() + "]");
    }
}
