package x.core.entity;

import x.core.floor.Dungeon;

public class Wall extends Entity {

    public Wall() {
        super(EntityType.WALL);
    }

    @Override
    public String getTileID() {
        return "wall.jpg";
    }

}
