package x.core.entity;

import x.core.floor.Dungeon;

public class Player extends Entity {

    public Player() {
        super(EntityType.PLAYER);
    }

    @Override
    public String getTileID() {
        return "player.png";
    }

    @Override
    public boolean playerEntered(int dx, int dy, Dungeon dungeon) {
        throw new IllegalStateException();
    }
}
