package x.core.entity;

import x.core.GraphicObject;
import x.core.floor.Dungeon;

public abstract class Entity implements GraphicObject {

    private final EntityType type;

    public Entity(EntityType type) {
        this.type = type;
    }

    public EntityType getType() {
        return type;
    }

    public boolean playerEntered(int dx, int dy, Dungeon dungeon) {
        // nop
        // do not interrupt other entity processing
        return false;
    }

    public void action(Dungeon dungeon) {
        // nop
    }
}
