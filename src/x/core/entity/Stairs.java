package x.core.entity;

import x.core.Coord;
import x.core.Game;
import x.core.floor.Dungeon;

public class Stairs extends Entity {

    private final Dungeon target;
    private final Coord targetCoord;

    public Stairs(Dungeon target, Coord targetCoord) {
        super(EntityType.STAIRS);
        this.target = target;
        this.targetCoord = targetCoord;
    }

    @Override
    public String getTileID() {
        return "stairs.png";
    }

    @Override
    public void action(Dungeon dungeon) {
        Game game = dungeon.getGame();
        game.setDungeon(this.target);
        game.getDungeon().transferPlayer(targetCoord.x, targetCoord.y);
    }
}
