package x.core.entity;

import x.core.Coord;
import x.core.floor.Dungeon;

public class Walkable extends Entity {

    public Walkable() {
        super(EntityType.WALKABLE);
    }

    @Override
    public String getTileID() {
        return null;
    }

    @Override
    public boolean playerEntered(int dx, int dy, Dungeon dungeon) {
        Coord player = dungeon.getPlayer();
        dungeon.transferPlayer(player.x + dx, player.y + dy);
        return false;
    }
}
