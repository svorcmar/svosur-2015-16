package x.core.entity;

public enum EntityType {

    ENEMY, PLAYER, STAIRS, WALKABLE, WALL, WATER
}
