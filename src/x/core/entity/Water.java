package x.core.entity;

import x.core.floor.Dungeon;

public class Water extends Entity {
    
    public Water() {
        super(EntityType.WATER);
    }

    @Override
    public String getTileID() {
        return "water.jpg";
    }

}
