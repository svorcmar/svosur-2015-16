package x.core;


import java.awt.*;

public interface Message {

    void drawSelf(Graphics2D g, int x, int y);

}
