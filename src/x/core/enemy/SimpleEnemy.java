package x.core.enemy;

import x.core.System_message;
import x.core.floor.Dungeon;

public class SimpleEnemy extends AbstractEnemy {

    @Override
    public boolean playerEntered(int dx, int dy, Dungeon dungeon) {
        dungeon.getGame().getConsole().addMessage(new System_message("ENEMY CONTACT - IMPLEMENT!"));
        // suppress other processing - enemy blocks the way
        return true;
    }

    @Override
    public String getTileID() {
        return "ghost.png";
    }

    @Override
    public void tick(Dungeon dungeon) {
        dungeon.getGame().getConsole().addMessage(new System_message("There is an enemy present in this level"));
    }

}
