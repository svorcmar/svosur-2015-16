package x.core.enemy;

import x.core.Ticking;
import x.core.entity.Entity;
import x.core.entity.EntityType;
import x.core.floor.Dungeon;

public abstract class AbstractEnemy extends Entity implements Ticking {

    protected boolean alive = true;

    public AbstractEnemy() {
        super(EntityType.ENEMY);
    }

    @Override
    public void tick(Dungeon dungeon) {
        // nothing
    }

    @Override
    public boolean isAlive() {
        return alive;
    }
}
