package x.core.light;

import x.core.Coord;

/**
 * Zdroj svetla, co osvetluje kruh.
 */
public class CircularLightSource implements LightSource {

    // stred kruhu
    private final Coord center;
    // centered at center coordinate
    private float[][] lightCircle;
    private int radius;

    public CircularLightSource(int radius, int x, int y) {
       this (radius, new Coord(x, y));
    }

    public CircularLightSource(int radius, Coord center) {
        this.center = center;
        this.radius = radius;
        lightCircle = new float[2 * radius + 1][2 * radius + 1];
        for (int i = -radius; i <= radius; i++) {
            for (int j = -radius; j <= radius; j++) {
                double squareDst = j * j + i * i;
                if (squareDst < radius * radius) {
                    lightCircle[i + radius][j + radius] = (float) (1 - Math.sqrt(squareDst) / radius);
                }
            }
        }
    }

    @Override
    public float getIntensity(int x, int y) {
        x = center.x + radius - x;
        y = center.y + radius - y;
        if (x >= 0 && x < lightCircle.length && y >= 0 && y < lightCircle[x].length)
            return lightCircle[x][y];
        else
            return 0;
    }

    public Coord getCenter() {
        return center;
    }

    public int getRadius() {
        return radius;
    }
}
