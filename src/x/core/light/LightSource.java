package x.core.light;

/**
 * Zdroj svetla.
 */
public interface LightSource {

    // Vrati intenzitu, s jakou tento zdroj osvetluje pole na souradnicich [x, y]
    float getIntensity(int x, int y);

}
