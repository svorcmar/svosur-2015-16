package x.core.paint;

import x.core.Coord;
import x.core.DungeonSquare;
import x.core.entity.Entity;
import x.core.floor.Dungeon;
import x.core.util.ImageUtils;

import javax.swing.*;
import java.awt.*;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DungeonPanel extends JPanel {

    public static final double MIN_LIGHT = 0.88;

    private int BASE_TILE_SIZE_PX = 64;

    private static final boolean SEE_ALL = false;

    /* šířka a výška panelu měřená v políčkách */
    private int width;
    private int height;
    private final double heightRatio;

    private double panelWidthPx;
    private double panelHeightPx;

    private Dungeon dungeon;
    /* souřadnice hráče uvnitř panelu - [0 až width][0 až height] */
    private Coord player;
    /* poslední známé souřadnice hráče uvnitřdungeonu */
    private final Coord lastPlayer;
    /* dorovnání hráče (pro účely zoomu) */
    private final Coord finePlacement;

    private int minPlayerX, maxPlayerX, minPlayerY, maxPlayerY;

    /* velikost jednoho tilu v px, může se měnit při zoomování */
    private Integer tileSizePx;
    private ImageUtils imageUtils;

    public DungeonPanel(int height, double heightRatio, Dungeon dungeon) {
        super(true);
        this.height = height;
        this.heightRatio = heightRatio;
        lastPlayer = new Coord();
        finePlacement = new Coord(0, 0);
        setDungeon(dungeon);
    }

    public void setDungeon(Dungeon dungeon) {
        this.dungeon = dungeon;
        Coord player = dungeon.getPlayer();
        lastPlayer.x = player.x;
        lastPlayer.y = player.y;
        initialize();
    }

    private void initialize() {
        // player coords relative to the display
        // measure the screen
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        panelWidthPx = screenSize.getWidth();
        double screenSizeHeight = screenSize.getHeight();
        panelHeightPx = heightRatio * screenSizeHeight;
        // compute ratio
        if (tileSizePx == null) {
            // height driven
            // not pixel perfect ratio
            tileSizePx = (int) Math.round(panelHeightPx / height);
        } else {
            // tile size driven
            height = (int) Math.ceil(panelHeightPx / tileSizePx);
        }
        double ratio = 1.0 * tileSizePx / BASE_TILE_SIZE_PX;

        if (height * tileSizePx < panelHeightPx)
            ++height;

        width = (int) Math.ceil(panelWidthPx / tileSizePx);

        player = new Coord(width / 2, this.height / 2);
        // inicializace boundaries posunu
        int tilesMoveBoundary = 2;
        minPlayerX = tilesMoveBoundary;
        maxPlayerX = width - tilesMoveBoundary - 1;
        minPlayerY = tilesMoveBoundary;
        maxPlayerY = height - tilesMoveBoundary - 1;
        // škálování obrázků
        imageUtils = new ImageUtils(ratio);
    }

    @Override
    public void paintComponent(Graphics origG) {
        Graphics2D g;
        // přepočet souřadic hráče - má se posunout vzhledem k panelu?
        Coord newPlayer = dungeon.getPlayer();
        int newX = player.x + newPlayer.x - lastPlayer.x;
        int newY = player.y + newPlayer.y - lastPlayer.y;
        lastPlayer.x = newPlayer.x;
        lastPlayer.y = newPlayer.y;
        if (newX >= minPlayerX && newX <= maxPlayerX && newY >= minPlayerY && newY <= maxPlayerY) {
            player.x = newX;
            player.y = newY;
        }

        // chceme hrace uprostred obrazovky
        int xShift = newPlayer.x - this.player.x;
        int yShift = newPlayer.y - this.player.y;
        // musi se prepocitat osvetleni? pokud ano, prepocitej
        if (dungeon.shouldRecomputeLighting()) {
            dungeon.clearLighting();
            dungeon.updateVisibilities();
            dungeon.computeLighting(newPlayer.x, newPlayer.y);
            dungeon.lightingRecomputed();
        }

        // Graphics2D pro tiles - bez škálování
        g = (Graphics2D) origG.create();

        List<Image> images;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                // hodnota zatmavení - 0 žádné, 1 úplné
                double darkening;
                int x = i + xShift;
                int y = j + yShift;
                if (dungeon.isWithin(x, y)) {
                    //image = getImageForTile(dungeon.get(x, y));
                    DungeonSquare tile = dungeon.get(x, y);
                    if (tile.isExplored() || SEE_ALL) {
                        // pole je bud prave viditelne nebo v minulosti prozkoumane
                        // ziskej zakladni barvu
                        images = getImageForTile(tile);
                        // viditelne? (pozor, nezamenovat s osvetleni - i osvetlene pole nemusi byt hracem videt, pokud je za zdi)
                        if (dungeon.lineOfSightBlocked(x, y)) {
                            // todo - chováme se stejně, jako pokud je neprozkoumáno
                            darkening = MIN_LIGHT;
                        } else {
                            darkening = Math.min(1 - tile.getLightIntensity(), MIN_LIGHT);
                        }
                    } else {
                        images = Collections.emptyList();
                        darkening = 1;
                    }
                } else {
                    images = Collections.emptyList();
                    darkening = 1;
                }
                g.drawImage(getFloor(), i * tileSizePx, (height - j - 1) * tileSizePx, null);
                for (Image image : images) {
                    g.drawImage(image, i * tileSizePx, (height - j - 1) * tileSizePx, null);
                }
                // dark overlay
                g.setColor(new Color(0f, 0f, 0f, (float) darkening));
                g.fillRect(i * tileSizePx, (height - j - 1) * tileSizePx, tileSizePx, tileSizePx); // todo proc +1
            }
        }
    }

    private Image getFloor() {
        return imageUtils.getScaledImage("floor.jpg");
    }

    private List<Image> getImageForTile(DungeonSquare square) {
        if (square != null) {
            List<Entity> entities = square.getEntities();
            if (entities.size() == 0)
                return Collections.emptyList();
            else {
                return entities.stream().map(e -> imageUtils.getScaledImage(e.getTileID())).collect(Collectors.toList());
            }
        }
        throw new NullPointerException();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension((int)panelWidthPx, (int)panelHeightPx);
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }

    @Override
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }

    public int getTileSizePx() {
        return tileSizePx;
    }

    public void setTileSizePx(int tileSizePx) {
        this.tileSizePx = tileSizePx;
        initialize();
    }
}
