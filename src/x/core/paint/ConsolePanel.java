package x.core.paint;

import x.core.GameConsole;
import x.core.Message;

import javax.swing.*;
import java.awt.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.List;

public class ConsolePanel extends JPanel {

    private final GameConsole console;
    private final JScrollPane wrapper;
    private int fontHeight;

    private volatile int prevContentSize = 0;

    public ConsolePanel(GameConsole console) {
        this.console = console;
        this.wrapper = new JScrollPane(this, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    }

    @Override
    protected void paintComponent(Graphics g) {
        List<Message> content = console.getContent();
        if (content.size() != prevContentSize) {
            // message change, update scroll pane + scroll down
            revalidate();
            prevContentSize = content.size();
            scrollToBottom();
        }
        super.paintComponent(g);
        fontHeight = g.getFontMetrics().getHeight();
        int offset = 0;
        for (Message row : content) {
            row.drawSelf((Graphics2D) g, 500, 20 + offset);
            offset += fontHeight;
        }
    }

    private void scrollToBottom() {
        JScrollBar verticalBar = wrapper.getVerticalScrollBar();
        AdjustmentListener downScroller = new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                Adjustable adjustable = e.getAdjustable();
                adjustable.setValue(adjustable.getMaximum());
                verticalBar.removeAdjustmentListener(this);
            }
        };
        verticalBar.addAdjustmentListener(downScroller);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, console.getContent().size() * fontHeight);
    }

    @Override
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }

    public JScrollPane getWrapper() {
        return wrapper;
    }
}
