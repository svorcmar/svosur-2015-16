package x.core;

import x.core.floor.Dungeon;
import x.core.floor.DungeonReader;
import x.core.paint.ConsolePanel;
import x.core.paint.DungeonPanel;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;

public class Game {

    private static final boolean MANUAL_FLOOR = true;
    private final double dpHeightRatio = 0.7;

    private JFrame window;
    private DungeonPanel dungeonPanel;
    private Dungeon dungeon;
    private GameConsole console = new GameConsole();

    public Game() throws Exception {

        if (MANUAL_FLOOR) {
            String input = "Floor 1\n" +
                    "WWWWWWWWWWWW\n" +
                    "W     P    W\n" +
                    "WW        WW\n" +
                    "WXW      W W\n" +
                    "W  W    W  W\n" +
                    "W   W  W   W\n" +
                    "W W W  W W W\n" +
                    "W W W  W W W\n" +
                    "W W W  W W W\n" +
                    "W W   S  W W\n" +
                    "WwwwwwwwwwwW\n" +
                    "WWWWWWWWWWWW\n";
            DungeonReader reader = new DungeonReader(new BufferedReader(new StringReader(input)), this);
            dungeon = reader.readFloor();
        } else {
            dungeon = new Dungeon(this, 100, 100, "Random dungeon");
        }
        dungeonPanel = new DungeonPanel(15, dpHeightRatio, dungeon);
    }

    public void setDungeon(Dungeon dungeon) {
        this.dungeon = dungeon;
        dungeonPanel.setDungeon(this.dungeon);
    }

    public static void main(String[] args) throws Exception {
        Game game = new Game();
        game.start();
    }

    private void start() {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    window = new JFrame("Dungeon");
                    LayoutManager manager = new BoxLayout(window.getContentPane(), BoxLayout.Y_AXIS);
                    window.add(dungeonPanel);

                    ConsolePanel consolePanel = new ConsolePanel(console);
                    JScrollPane scroller = consolePanel.getWrapper();

//                    consolePanel.setBorder(new LineBorder(Color.RED, 5));
//                    scroller.setBorder(new LineBorder(Color.GREEN, 5));
//                    dungeonPanel.setBorder(new LineBorder(Color.RED, 5));

                    window.add(scroller);
                    window.setLayout(manager);
                    window.setUndecorated(true);
                    window.setExtendedState(JFrame.MAXIMIZED_BOTH);
                    window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

                    window.pack();
                    window.setVisible(true);
                    window.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyPressed(KeyEvent e) {
                            int dx = 0, dy = 0, zoom = 0;
                            boolean action = false;
                            switch (e.getKeyCode()) {
                                case KeyEvent.VK_W:
                                    dy = 1;
                                    action = true;
                                    break;
                                case KeyEvent.VK_S:
                                    dy = -1;
                                    action = true;
                                    break;
                                case KeyEvent.VK_A:
                                    dx = -1;
                                    action = true;
                                    break;
                                case KeyEvent.VK_D:
                                    dx = 1;
                                    action = true;
                                    break;
                                case KeyEvent.VK_ADD:
                                    zoom++;
                                    break;
                                case KeyEvent.VK_SUBTRACT:
                                    zoom--;
                                    break;
                                case KeyEvent.VK_E:
                                    action = true;
                                    dungeon.action();
                                    window.repaint();
                                    Message message = new NewFloorMsg(getDungeon());
                                    getConsole().addMessage(message);
                                    break;
                            }
                            if (action) {
                                dungeon.tick();
                            }
                            if (dx != 0 || dy != 0) {
                                dungeon.movePlayer(dx, dy);
                            }
                            if (zoom != 0) {
                                dungeonPanel.setTileSizePx(dungeonPanel.getTileSizePx() + zoom);
                            }
                            repaintAll();
                        }
                    });
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void repaintAll() {
        window.revalidate();
        window.repaint();
    }

    public GameConsole getConsole() {
        return console;
    }

    public Dungeon getDungeon() {
        return dungeon;
    }
}
