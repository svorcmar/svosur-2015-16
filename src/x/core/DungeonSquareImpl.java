package x.core;

import x.core.entity.Entity;
import x.core.entity.EntityType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DungeonSquareImpl implements DungeonSquare {

    private float lightIntensity = 0;
    private boolean explored;
    public final List<Entity> entities = new ArrayList<>();


    @Override
    public float getLightIntensity() {
        return lightIntensity;
    }

    @Override
    public boolean isExplored() {
        return explored;
    }

    @Override
    public void setExplored(boolean explored) {
        this.explored = explored;
    }

    @Override
    public List<Entity> getEntities() {
        return entities;
    }

    @Override
    public void addEntity(Entity e) {
        if (e != null)
            this.entities.add(e);
    }

    @Override
    public boolean removeEntity(Entity e) {
        return entities.remove(e);
    }

    @Override
    public boolean hasEntity(EntityType type) {
        return entities.stream().anyMatch(e -> e.getType() == type);
    }

    @Override
    public int removeEntity(EntityType type) {
        List<Entity> collect = entities.stream().filter(e -> e.getType() == type).collect(Collectors.toList());
        entities.removeAll(collect);
        return collect.size();
    }

    @Override
    public void clearEntities() {
        entities.clear();
    }

    @Override
    public void setLightIntensity(float level, int mode) {
        switch (mode) {
            case 0:
                // overwrite
                lightIntensity = level;
                break;
            case 1:
                lightIntensity = Math.min(1, lightIntensity + level);
                break;
            case 2:
                // higher overwrite
                lightIntensity = Math.max(level, lightIntensity);
                break;
            case 3:
                // lower overwrite
                lightIntensity = Math.min(level, lightIntensity);
                break;
            default:
                throw new IllegalArgumentException("Unknown mode: " + mode);
        }
    }

    @Override
    public boolean isOpaque() {
        return entities.stream().anyMatch(e -> e.getType() == EntityType.WALL);
    }
}
