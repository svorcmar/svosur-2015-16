package x.core;

import x.core.entity.Entity;
import x.core.entity.EntityType;

import java.util.List;

public interface DungeonSquare {

    int MAX_VISIBILITY_LEVEL = 64;

    float getLightIntensity();

    boolean isExplored();

    void setExplored(boolean explored);

    List<Entity> getEntities();

    void addEntity(Entity e);

    boolean removeEntity(Entity e);

    int removeEntity(EntityType type);

    void clearEntities();

    void setLightIntensity(float level, int mode);

    boolean isOpaque();

    boolean hasEntity(EntityType type);
}
