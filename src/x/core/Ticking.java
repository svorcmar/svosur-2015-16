package x.core;

import x.core.floor.Dungeon;

public interface Ticking {

    void tick(Dungeon dungeon);

    boolean isAlive();
}
